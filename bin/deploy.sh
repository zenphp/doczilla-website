# Enable strict error checking
set -euo pipefail

# Path variables
DEPLOY_DIR="/home/forge"
DOCS_DIR="${DEPLOY_DIR}/doczil.la"
NEW_DOCS_DIR="${DEPLOY_DIR}/newdocs"
BACKUPS_DIR="${DEPLOY_DIR}/doczilla_backups"
REPO_URL="git@gitlab.com:zenphp/doczil-la.git"

# Clone the repository to a new directory
if ! git clone ${REPO_URL} ${NEW_DOCS_DIR}; then
    echo "Failed to clone repository. Deployment aborted."
    exit 1
fi

# Copy the .env and maintenance mode file to the new directory
cp ${DOCS_DIR}/.env ${NEW_DOCS_DIR}/

# Change to the new directory and check for package.json
cd ${NEW_DOCS_DIR}
if [ ! -f package.json ]; then
    echo "Error: package.json not found. Cannot proceed with the deployment."
    exit 1
fi

# NPM operations
npm install || { echo "Failed to install npm packages"; exit 1; }
npm run pull:docs || { echo "Failed to pull docs"; exit 1; }
npm run build || { echo "Build failed"; exit 1; }

# Manage backups
backup_count=$(ls -1 ${BACKUPS_DIR} | wc -l)
if [ "$backup_count" -ge 5 ]; then
    oldest_backup=$(ls -t ${BACKUPS_DIR} | tail -1)
    rm -rf "${BACKUPS_DIR}/${oldest_backup}"
    echo "Oldest backup removed: $oldest_backup"
fi

# Rotate directories
timestamp=$(date +%Y-%m-%d-%H-%M-%S)
mv ${DOCS_DIR} ${BACKUPS_DIR}/docs-$timestamp
mv ${NEW_DOCS_DIR} ${DOCS_DIR}

# Release Notes

## [1.0.2](https://gitlab.com/zenphp/doczilla-website/compare/v1.0.1...v1.0.2) (2024-05-08)


### Code Refactoring

* removed header link and updated home ([c32409d](https://gitlab.com/zenphp/doczilla-website/commit/c32409d123a514a4e2904f8b7da432fa0887430c))

## [1.0.1](https://gitlab.com/zenphp/doczilla-website/compare/v1.0.0...v1.0.1) (2024-05-08)


### Bug Fixes

* missing express dependency ([dcebc59](https://gitlab.com/zenphp/doczilla-website/commit/dcebc5999bd64e3022ab1f4ab22aaca335791d86))

# 1.0.0 (2024-05-08)


### Breaking

* initial commit ([70e2b37](https://gitlab.com/zenphp/doczilla-website/commit/70e2b3758fe4c15b412e3e2e7abf4dd907c508ce))
* initial commit for new website ([2c6cfbc](https://gitlab.com/zenphp/doczilla-website/commit/2c6cfbc4672528905361d9bf76c309fba544aeb3))


### Bug Fixes

* grids, sticky header, scroll issues ([5de9689](https://gitlab.com/zenphp/doczilla-website/commit/5de96891b679159a66a2a54b9d9753c719bc139d))
* missing search type key in config ([bf486cc](https://gitlab.com/zenphp/doczilla-website/commit/bf486cc8a6335807b21a74398baaf65951e9b11b))
* un-ignore maintenance.md ([aca4f74](https://gitlab.com/zenphp/doczilla-website/commit/aca4f7438085f48c7f255b2b2acee324baf576e6))
* updates meilisearch to prevent flash ([4e33a94](https://gitlab.com/zenphp/doczilla-website/commit/4e33a94058df9a8d83fa95aadfc644437d67f626))
* updates to fix installation ([2e790b3](https://gitlab.com/zenphp/doczilla-website/commit/2e790b3f42477c93c0836585fb3498320d3b63d7))


### Code Refactoring

* add elements from live production tests ([503cd12](https://gitlab.com/zenphp/doczilla-website/commit/503cd123248044e03636f08f39db836893a85e7f))
* remove typography plugin new css ([513e82a](https://gitlab.com/zenphp/doczilla-website/commit/513e82a8dd4d0b6f2abdeb3b88bf67a931f91182))


### Maintenance

* code styles ([e20a9be](https://gitlab.com/zenphp/doczilla-website/commit/e20a9bed13b05e29210fc9cb06aaf5562269d2f4))
* **release:** 1.0.0 [skip ci] ([00730c3](https://gitlab.com/zenphp/doczilla-website/commit/00730c36f1185ac460df04a66dbd16d715868450))
* **release:** 1.0.1 [skip ci] ([2a6bcbf](https://gitlab.com/zenphp/doczilla-website/commit/2a6bcbf1b2a9efbd10b0f9390f9d75139273c73f))
* **release:** 1.0.2 [skip ci] ([f63f53e](https://gitlab.com/zenphp/doczilla-website/commit/f63f53ec46750e416548c8c377571242b552291c))
* **release:** 1.0.3 [skip ci] ([2b91f9b](https://gitlab.com/zenphp/doczilla-website/commit/2b91f9b156a2a10dbb2880ed191fde0f627ac76f))
* **release:** 1.0.4 [skip ci] ([3f8663e](https://gitlab.com/zenphp/doczilla-website/commit/3f8663e836aaa950bc2b5728d117f4714157bb1e))
* **release:** 1.0.5 [skip ci] ([7143aff](https://gitlab.com/zenphp/doczilla-website/commit/7143aff3d787a7f8767268e7e022b0301c3c022e))
* **release:** 1.0.6 [skip ci] ([a5d286b](https://gitlab.com/zenphp/doczilla-website/commit/a5d286b208c6eb8d2d785efe35852ec588cca5e6))
* **release:** 1.0.7 [skip ci] ([09bb848](https://gitlab.com/zenphp/doczilla-website/commit/09bb848f2c40b68d14ba613cd77110869f8b7784))


### Work in Progress

* additional changes for 1.0 ([62dca79](https://gitlab.com/zenphp/doczilla-website/commit/62dca792a7eea9904a80a1ba49191bff152151bb))
* continue setting up repository for 1.0 ([a0568aa](https://gitlab.com/zenphp/doczilla-website/commit/a0568aa3e31ba992d4fa8501553c0053edcd1147))
* restyling for css grid and home page ([0f36dc7](https://gitlab.com/zenphp/doczilla-website/commit/0f36dc7f1225cb132a81b22b3a005d165284aeb1))

## [1.0.7](https://gitlab.com/zenphp/doczilla/compare/v1.0.6...v1.0.7) (2024-05-08)


### Bug Fixes

* updates meilisearch to prevent flash ([4e33a94](https://gitlab.com/zenphp/doczilla/commit/4e33a94058df9a8d83fa95aadfc644437d67f626))

## [1.0.6](https://gitlab.com/zenphp/doczilla/compare/v1.0.5...v1.0.6) (2024-05-08)


### Bug Fixes

* missing search type key in config ([bf486cc](https://gitlab.com/zenphp/doczilla/commit/bf486cc8a6335807b21a74398baaf65951e9b11b))

## [1.0.5](https://gitlab.com/zenphp/doczilla/compare/v1.0.4...v1.0.5) (2024-05-08)


### Bug Fixes

* un-ignore maintenance.md ([aca4f74](https://gitlab.com/zenphp/doczilla/commit/aca4f7438085f48c7f255b2b2acee324baf576e6))

## [1.0.4](https://gitlab.com/zenphp/doczilla/compare/v1.0.3...v1.0.4) (2024-05-08)


### Code Refactoring

* add elements from live production tests ([503cd12](https://gitlab.com/zenphp/doczilla/commit/503cd123248044e03636f08f39db836893a85e7f))

## [1.0.3](https://gitlab.com/zenphp/doczilla/compare/v1.0.2...v1.0.3) (2024-04-03)


### Code Refactoring

* remove typography plugin new css ([513e82a](https://gitlab.com/zenphp/doczilla/commit/513e82a8dd4d0b6f2abdeb3b88bf67a931f91182))

## [1.0.2](https://gitlab.com/zenphp/doczilla/compare/v1.0.1...v1.0.2) (2024-04-01)


### Bug Fixes

* grids, sticky header, scroll issues ([5de9689](https://gitlab.com/zenphp/doczilla/commit/5de96891b679159a66a2a54b9d9753c719bc139d))


### Work in Progress

* restyling for css grid and home page ([0f36dc7](https://gitlab.com/zenphp/doczilla/commit/0f36dc7f1225cb132a81b22b3a005d165284aeb1))

## [1.0.1](https://gitlab.com/zenphp/doczilla/compare/v1.0.0...v1.0.1) (2024-03-29)


### Bug Fixes

* updates to fix installation ([2e790b3](https://gitlab.com/zenphp/doczilla/commit/2e790b3f42477c93c0836585fb3498320d3b63d7))


### Work in Progress

* additional changes for 1.0 ([62dca79](https://gitlab.com/zenphp/doczilla/commit/62dca792a7eea9904a80a1ba49191bff152151bb))

# 1.0.0 (2024-03-28)


### Breaking

* initial commit ([70e2b37](https://gitlab.com/zenphp/doczilla/commit/70e2b3758fe4c15b412e3e2e7abf4dd907c508ce))


### Maintenance

* code styles ([e20a9be](https://gitlab.com/zenphp/doczilla/commit/e20a9bed13b05e29210fc9cb06aaf5562269d2f4))


### Work in Progress

* continue setting up repository for 1.0 ([a0568aa](https://gitlab.com/zenphp/doczilla/commit/a0568aa3e31ba992d4fa8501553c0053edcd1147))
